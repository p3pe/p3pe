# P (FREE) P ECONOMY

## GIST

what if the sharing economy was really about sharing?

## STATUS QUO

Currently, the 'p2p economy' sharing economy is about sharing resources and
allocating resources efficiently, between those who have time / resources and
those who have money while trying to [drive down the cost of labor in lieu of
value to the consumer and shareholders][1]

## WHAT COULD BE

instead of corporations centralizing around profit (for the business), what if
the value of the business is shared among all stake holders?

it's long been thought that 'shareholders' (in the sense of those who own share
of the stock) are not the individuals invested in the long term future of the
company; [those are the workers, the owners, the clients and the greater
community.][2]

## PLAN

create and understand a system for sharing both profits and resources among the
workers and community of a task based economy. instead of losing autonomy as
technology transfers wealth to the rich, use technology to create
self-sufficient organizations.


## NEXT STEPS

* collect data / operating costs for companies already in this space
* collect feedback
* put forward proposals for ways the same set of resources could be managed
* understand the risk of different proposals and hidden assumptions
* simulate scenarios for proposals (optional)
* collect feedback

### LOOKING FOR

* coders
* MBAs
* marketers
* workers

### HOW YOU CAN HELP

Write it! Tell others about the idea. Stop using profit centric services.

## BENEFITS

maybe we move forward as a society. maybe we share some ideas. maybe we sink
some services that represent gentrification and class inequality. who knows.

more concretely, though - what if work was at will and consists of shared
benefits (health, vacation, etc), instead of being under steady duress?


## KNOWN UNKNOWNS 

* what does it take to vet a worker?
* what are applicable profit sharing models?
* who knows much more than myself about this?

## RISKS (AND DOWNSIDES)

* TBD

## DESIRED OUTCOMES

* TBD



[1]: http://www.salon.com/2015/02/04/robert_reich_the_sharing_economy_is_hurtling_us_backwards_partner
[2]: http://en.wikipedia.org/wiki/Shareholder_value#Maximizing_shareholder_value
